using System.ComponentModel.DataAnnotations.Schema;

namespace estados_prod
{
    [Table("estados")]
    public class Estado
    {
        public int id { get; set; }

        public string uf { get; set; }

        public string nome { get; set; }
    }
}