﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace estados_prod.Migrations
{
    public partial class addDados : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "estados",
                columns: new[] { "id", "nome", "uf" },
                values: new object[,]
                {
                    { 1, "Acre", "AC" },
                    { 25, "São Paulo", "SP" },
                    { 24, "Santa Catarina", "SC" },
                    { 23, "Roraima", "RR" },
                    { 22, "Rondônia", "RO" },
                    { 21, "Rio Grande do Sul", "RS" },
                    { 20, "Rio Grande do Norte", "RN" },
                    { 19, "Rio de Janeiro", "RJ" },
                    { 18, "Piauí", "PI" },
                    { 17, "Pernambuco", "PE" },
                    { 16, "Paraná", "PR" },
                    { 15, "Paraíba", "PB" },
                    { 26, "Sergipe", "SE" },
                    { 14, "Pará", "PA" },
                    { 12, "Mato Grosso do Sul", "MS" },
                    { 11, "Mato Grosso", "MT" },
                    { 10, "Maranhão", "MA" },
                    { 9, "Goiás", "GO" },
                    { 8, "Espírito Santo", "ES" },
                    { 7, "Distrito Federal", "DF" },
                    { 6, "Ceará", "CE" },
                    { 5, "Bahia", "BA" },
                    { 4, "Amazonas", "AM" },
                    { 3, "Amapá", "AP" },
                    { 2, "Alagoas", "AL" },
                    { 13, "Minas Gerais", "MG" },
                    { 27, "Tocantins", "TO" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "estados",
                keyColumn: "id",
                keyValue: 27);
        }
    }
}
