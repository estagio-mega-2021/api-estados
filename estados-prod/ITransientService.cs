using System.Collections.Immutable;

namespace estados_prod
{
    public interface ITransientService
    {
        ImmutableList<Estado> Get();
        int Post(Estado estados);
    }
}