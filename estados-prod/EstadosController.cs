using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace estados_prod
{
    [ApiController]
    [Route("[controller]")]
    public class EstadosController : ControllerBase
    {
        ITransientService _serviceEstados;

        public EstadosController(ITransientService service)
        {
            _serviceEstados = service;
        }

        [HttpPost]
        public int Post(Estado estados)
        {
            return _serviceEstados.Post(estados);
        }

        [HttpGet]
        public ImmutableList<Estado> Get()
        {
            return _serviceEstados.Get();
        }
    }
}