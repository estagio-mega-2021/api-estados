using Microsoft.EntityFrameworkCore;
namespace estados_prod
{
    public class EstadoDbContext : DbContext
    {
        public DbSet<Estado> Estados { get; set; }

        public EstadoDbContext(DbContextOptions options) : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estado>().HasData(
            new Estado {id =1, uf ="AC", nome ="Acre"},
            new Estado {id =2, uf ="AL", nome ="Alagoas"},
            new Estado {id =3, uf ="AP", nome ="Amapá"},
            new Estado {id =4, uf ="AM", nome ="Amazonas"},
            new Estado {id =5, uf ="BA", nome ="Bahia"},
            new Estado {id =6, uf ="CE", nome ="Ceará"},
            new Estado {id =7, uf ="DF", nome ="Distrito Federal"},
            new Estado {id =8, uf ="ES", nome ="Espírito Santo"},
            new Estado {id =9, uf ="GO", nome ="Goiás"},
            new Estado {id =10, uf ="MA", nome ="Maranhão"},
            new Estado {id =11, uf ="MT", nome ="Mato Grosso"},
            new Estado {id =12, uf ="MS", nome ="Mato Grosso do Sul"},
            new Estado {id =13, uf ="MG", nome ="Minas Gerais"},
            new Estado {id =14, uf ="PA", nome ="Pará"},
            new Estado {id =15, uf ="PB", nome ="Paraíba"},
            new Estado {id =16, uf ="PR", nome ="Paraná"},
            new Estado {id =17, uf ="PE", nome ="Pernambuco"},
            new Estado {id =18, uf ="PI", nome ="Piauí"},
            new Estado {id =19, uf ="RJ", nome ="Rio de Janeiro"},
            new Estado {id =20, uf ="RN", nome ="Rio Grande do Norte"},
            new Estado {id =21, uf ="RS", nome ="Rio Grande do Sul"},
            new Estado {id =22, uf ="RO", nome ="Rondônia"},
            new Estado {id =23, uf ="RR", nome ="Roraima"},
            new Estado {id =24, uf ="SC", nome ="Santa Catarina"},
            new Estado {id =25, uf ="SP", nome ="São Paulo"},
            new Estado {id =26, uf ="SE", nome ="Sergipe"},
            new Estado {id =27, uf ="TO", nome ="Tocantins"});
            
        }
    }
    
}