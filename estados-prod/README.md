~~~ cs
dotnet ef migrations add <name of migration>
~~~

[Documentação Entity FrameWork - Migration](https://www.learnentityframeworkcore.com/migrations/seeding)

## Seeds
~~~ cs
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
    modelBuilder.Entity<Author>().HasData(
        new Author
        {
            AuthorId = 1,
            FirstName = "William",
            LastName = "Shakespeare"
        }
    );
}
~~~