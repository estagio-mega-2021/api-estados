using System.Collections.Immutable;

namespace estados_prod
{
    public class ServiceEstados : ITransientService
    {
        private EstadoDbContext _context;

        public ServiceEstados(EstadoDbContext context)
        {
            _context = context;
        }
        
        public ImmutableList<Estado> Get()
        {
            return _context.Estados.ToImmutableList();
        }
        
        public int Post(Estado estados)
        {
            _context.Estados.Add(estados);
            _context.SaveChanges();
            return estados.id;
        }

        public Estado Put(Estado estado)
        {
            Estado encontrado = Get().Find(Estado => Estado.id == estado.id);
            encontrado.nome = estado.nome;
            encontrado.uf = estado.uf;
            _context.Estados.Update(encontrado);
            _context.SaveChanges();
            return Get().Find(Estado => Estado.id == estado.id);
        }
        public Estado Delete(int id)
        {
            Estado encontrado = Get().Find(Estado => Estado.id == id);
            _context.Estados.Remove(encontrado);
            _context.SaveChanges();
            return Get().Find(Estado => Estado.id == id);
        }
    }
}