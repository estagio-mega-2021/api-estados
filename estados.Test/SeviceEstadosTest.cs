using Xunit;
using estados_prod;

namespace estados_prod.Test
{
    public class SeviceEstadosTest
    {
        
        [Fact]
        public void GetEstadosTest()
        {
            var novoDbContext = ContextTest.CriarDbContext("Can_get_items");
            ContextTest.Add_estados(novoDbContext);
            var service = new ServiceEstados(novoDbContext);
            Assert.NotEmpty(service.Get());
        }

        [Fact]
        public void PostEstadosTest()
        {
            var novoDbContext = ContextTest.CriarDbContext("Can_post_items");
            var service = new ServiceEstados(novoDbContext);
            Assert.NotNull(service.Post(new Estado()));
        }

        [Fact]
        public void PutEstadosTest()
        {
            var novoDbContext = ContextTest.CriarDbContext("Can_put_items");
            var service = new ServiceEstados(novoDbContext);

            Estado novoEstado = new Estado();
            novoEstado.nome = "Novo Estado";
            novoEstado.uf = "NE";
            service.Post(novoEstado);

            Estado estadoAtualizado = new Estado();
            estadoAtualizado.id = 1;
            estadoAtualizado.nome = "Estado Atualizado";
            estadoAtualizado.uf = "EA";

            Estado retorno = service.Put(estadoAtualizado);
            bool igual = true;
            if(estadoAtualizado.nome != retorno.nome || estadoAtualizado.uf != retorno.uf){igual = false;}

            Assert.True(igual);
        }

        [Fact]
        public void DeleteEstadosTest()
        {
            var novoDbContext = ContextTest.CriarDbContext("Can_delete_items");
            var service = new ServiceEstados(novoDbContext);

            Estado novoEstado = new Estado();
            novoEstado.nome = "Novo Estado";
            novoEstado.uf = "NE";
            service.Post(novoEstado);

            Estado retorno = service.Delete(1);
            Assert.Null(retorno);
        }
    }
}