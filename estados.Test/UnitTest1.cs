using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.EntityFrameworkCore.InMemory;
using Xunit;

namespace estados_prod.Test
{
    public class UnitTest1
    {
        [Fact]
        public void TesteGetEstado()
        {
            var criarDbContext = ContextTest.CriarDbContext("Can_get_items");

            ContextTest.Add_estados(criarDbContext);

            var immutableList = new EstadosController(new ServiceEstados(criarDbContext)).Get();

            Assert.NotEmpty(immutableList);

        }

        [Fact]
        public void TestPost()
        {
            var criarDbContext = ContextTest.CriarDbContext("Can_post_items");
            Estado estados = new Estado();
            int retorno = new EstadosController(new ServiceEstados(criarDbContext)).Post(estados);
            Assert.NotNull(retorno);
        }

    }
}
