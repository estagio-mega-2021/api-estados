using Microsoft.EntityFrameworkCore;
namespace estados_prod.Test
{
    public class ContextTest
    {
        internal static EstadoDbContext CriarDbContext(string name)
        {
            var options = new DbContextOptionsBuilder<EstadoDbContext>().UseInMemoryDatabase(name).Options;
            return new EstadoDbContext(options);
        }

        internal static void Add_estados(EstadoDbContext context)
        {
            context.Estados.Add(new Estado()
            {
                uf = "AC",
                nome = "Acre"
            });
            context.SaveChanges();
        }

    }
}